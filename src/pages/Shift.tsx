import React, { FunctionComponent, useEffect, useState } from "react";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import { makeStyles } from "@material-ui/core/styles";
import { getErrorMessage } from "../helper/error/index";
import { deleteShiftById, getShifts, postShifts } from "../helper/api/shift";
import DataTable from "react-data-table-component";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import { useHistory } from "react-router-dom";
import ConfirmDialog from "../components/ConfirmDialog";
import Alert from "@material-ui/lab/Alert";
import { Link as RouterLink } from "react-router-dom";
import { WeeklyCalendar } from 'react-week-picker';
import 'react-week-picker/src/lib/calendar.css';

const useStyles = makeStyles((theme) => ({
  //styling
  root: {
    minWidth: 275,
  },
  fab: {
    position: "absolute",
    bottom: 40,
    right: 40,
    backgroundColor: 'white',
    color: theme.color.turquoise
  },
}));

//declaration button data with actionButtonProps
interface ActionButtonProps {
  id: string;
  onDelete: () => void;//function
}

//const for week calendar
const handleNextWeek = (currenDate: any) =>{
  console.log(`current date: ${currenDate}`)
}

const [isClicked, setIsClicked] = useState(false);

const handleWeekCalendarPick = (startDate: any, endDate: any) =>{
  console.log(`current date: ${startDate} to ${endDate}`)
}

const ActionButton: FunctionComponent<ActionButtonProps> = ({
  id,
  onDelete,
}) => {
  return (
    //for wrapping all component using div 
    <div>
      <IconButton
      //Delete button
        size="small"
        aria-label="delete"
        component={RouterLink}
        to={`/shift/${id}/edit`}
        disabled={isClicked == true ? true : false}
      >
        <EditIcon fontSize="small" />
      </IconButton>
      <IconButton 
        size="small" 
        aria-label="delete" 
        onClick={() => onDelete()}
        disabled={isClicked == true ? true : false}
      >
        <DeleteIcon fontSize="small" />
      </IconButton>
    </div>
  );
};

const Shift = () => {
  //declare construction that can be recall in function below
  const classes = useStyles();
  const history = useHistory();

  const [rows, setRows] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [errMsg, setErrMsg] = useState("");

  const [selectedId, setSelectedId] = useState<string | null>(null);
  const [showDeleteConfirm, setShowDeleteConfirm] = useState<boolean>(false);
  const [deleteLoading, setDeleteLoading] = useState<boolean>(false);

  //For popup/modals/dialog that have an action to show or hide it
  const onDeleteClick = (id: string) => {
    setSelectedId(id);
    setShowDeleteConfirm(true);
  };

  const onCloseDeleteDialog = () => {
    setSelectedId(null);
    setShowDeleteConfirm(false);
  };

  //hit request API put in useEffect and async
  useEffect(() => {
    const getData = async () => {
      try {
        setIsLoading(true);//when not error then loading true but if it was error, loading not matter
        setIsClicked(true);
        setErrMsg("");
        //if the data was exist that will put in result as a "basket"
        const { results } = await getShifts();
        setRows(results);
      } catch (error) {
        const message = getErrorMessage(error);
        setErrMsg(message);
      } finally {
        setIsLoading(false);
        setIsClicked(false);
      }
    };
    //data can be more than 1 data so it can be array
    getData();
  }, []);

  //set column using component with sortable true
  const columns = [
    {
      name: "Name",
      selector: "name",
      sortable: true,
    },
    {
      name: "Date",
      selector: "date",
      sortable: true,
    },
    {
      name: "Start Time",
      selector: "startTime",
      sortable: true,
    },
    {
      name: "End Time",
      selector: "endTime",
      sortable: true,
    },
    {
      name: "Actions",
      cell: (row: any) => (
        <ActionButton id={row.id} onDelete={() => onDeleteClick(row.id)} />
      ),
    },
  ];

  //for delete data condition with async because if selected != null then
  //it will call delete request by id (selectedId)
  const deleteDataById = async () => {
    try {
      setDeleteLoading(true);
      setErrMsg("");

      if (selectedId === null) {
        throw new Error("ID is null");
      }

      console.log(deleteDataById);

      await deleteShiftById(selectedId);
      //for index format if there's any data that have been deleted so index data wil change the format
      //iwth splice
      const tempRows = [...rows];
      const idx = tempRows.findIndex((v: any) => v.id === selectedId);
      tempRows.splice(idx, 1);
      setRows(tempRows);
    } catch (error) {
      const message = getErrorMessage(error);
      setErrMsg(message);
    } finally {
      setDeleteLoading(false);
      onCloseDeleteDialog();
    }
  };

  const handleClickPost = async () => {
    var formData = new FormData()
    formData.append('data', rows.length())
    try{
      await postShifts(formData)
    }
    catch(error){
      const message = getErrorMessage(error);
      setErrMsg(message);
    }
  }
  return (
    //this is what will be in font of the website with recall from up
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <Card className={classes.root}>
          <CardContent>
            {errMsg.length > 0 ? (
              <Alert severity="error">{errMsg}</Alert>
            ) : (
              <></>
            )}
            <DataTable
            //make table with datatable
              title="Shifts"
              columns={columns}
              data={rows}
              pagination
              progressPending={isLoading}
            />
          </CardContent>
        </Card>
      </Grid>
      <WeeklyCalendar 
        //weekly calender
        onWeekPick = {handleWeekCalendarPick}
        jumpToCurrentWeekRequired = {true}
        onJumpToCurrentWeek = {handleNextWeek}
      />
      <Fab
        //adding shift
        size="medium"
        aria-label="add"
        className={classes.fab}
        onClick={() => history.push("/shift/add")} // go to shift form
        disabled={isClicked == true ? true : false}
      >
        <AddIcon />
      </Fab>
      <Fab
        //publish shift
        size="medium"
        aria-label="publish"
        className={classes.fab}
        onClick={isClicked}
        // disabled={getData!=null?true}
        onSubmit={handleClickPost}
      >
        Publish
      </Fab>
      <ConfirmDialog
        title="Delete Confirmation"
        description={`Do you want to delete this data ?`}
        onClose={onCloseDeleteDialog}
        open={showDeleteConfirm}
        onYes={deleteDataById}
        loading={deleteLoading}
      />
    </Grid>
  );
};

export default Shift;
