import React from "react";
import { Redirect } from "react-router-dom";

const Home = () => {
  //this is for push to path shift
  return <Redirect to="/shift" />;
};

export default Home;
